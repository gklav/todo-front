import { Component, OnInit, Input } from '@angular/core';

import { Todo } from '../todo';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent implements OnInit {

  constructor(private todoService: TodoService) { }

  myTodo : Todo = {
    id: 0,
    title: "",
    description: "",
    state: false,
    creationDate: "",
    doneDate: ""
  };

  @Input() todoCreated?: any;

  submitted = false;

  newTodo() {
    this.todoService.postTodo(this.myTodo)
      .subscribe(todoCreated => this.todoCreated = todoCreated);
    this.submitted = true;
  }


  ngOnInit(): void {
  }

}
