import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Todo } from './todo';
//import { TODOS } from './mock-todos';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private apiEndpoint = 'http://127.0.0.1:8080/api/v1/todo';

  constructor(private http: HttpClient) { }

  // Fetch all TODOs from server
  getTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.apiEndpoint);
  }

  // Fetch a TODO from server with a specific id
  getTodo(id: number): Observable<Todo> {
    return this.http.get<Todo>(this.apiEndpoint + "/?id=" + id );
  }

  // Create a TODO on the server side
  postTodo(todo: Todo): Observable<Object> {
    let body = {
      title: todo.title,
      description: todo.description
    }
    return this.http.post<Todo>(this.apiEndpoint, body);
  }

  // Update a TODO state and add a doneDate on the server side
  updateDoneTodo(id: number): Observable<Object> {
    return this.http.put<Todo>(this.apiEndpoint + "/?id=" + id, {} );
  }
}
