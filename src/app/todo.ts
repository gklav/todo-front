/**
* Inteface for todo
* A "todo" is a task. It can be done or not.
*
* params :
*   id (number): identifiant for a todo
*   title (string): title of the todo (eg: implement a proper documentation)
*   description (string): description of the todo
*   state (boolean): state of a todo. True if done, False if not.
*/
export interface Todo {
  id: number;
  title: string;
  description: string;
  state: boolean;
  creationDate: string;
  doneDate: string;
}
