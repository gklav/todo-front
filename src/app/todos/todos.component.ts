import { Component, OnInit } from '@angular/core';

import { Todo } from '../todo';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todos: Todo[] = [];
  dones: Todo[] = [];

  constructor(private todoService: TodoService) { }

  // get data from todoService and place done todos at the end of todos array
  getTodos(): void {
    this.todoService.getTodos()
      .subscribe(todos =>{
        todos.forEach(todo => {
          if(todo.state){
            this.dones.push(todo)
          } else {
            this.todos.push(todo)
          }
        });
      });
  }

  // Place the todo at the and of todo arrays
  onStateDone(todo: Todo): void{
    this.todoService.updateDoneTodo(todo.id).subscribe();
    let index = this.todos.indexOf(todo,0);
    this.todos.splice(index,1);
    this.dones.push(todo);
  }

  ngOnInit(): void {
    this.getTodos();
  }

  ngDoCheck(): void {
    // sort todos, oldest creationDate on bottom
    this.todos.sort((a, b) => {
      // check if dates not null, then sort array
      if(a.creationDate < b.creationDate) {
        return 1
      } else {
        return -1
      }
      return 0
    });


    this.dones.sort((a, b) => {
      // check if dates not null, then sort array
      if(a.doneDate < b.doneDate) {
        return -1
      } else {
        return 1
      }
      return 0
    });
  }
}
